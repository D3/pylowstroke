# pyLowStroke

If there should be any issues, don't hesitate to contact me at fhahnlei@cs.washington.edu

## Installation
The library has been tested with <code>python==3.8</code>.

```shell
git clone https://gitlab.inria.fr/D3/pylowstroke
cd pylowstroke
pip install -r requirements.txt
pip install .
```