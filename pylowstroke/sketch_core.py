import sys, os, inspect
import numpy as np
from math import acos
from rdp import rdp
from sklearn.decomposition import PCA
from sklearn import linear_model
from copy import deepcopy

from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
from matplotlib import pylab as pl
from matplotlib.collections import LineCollection

import pylowstroke.sketch_tools as Tools
import pylowstroke.sketch_shapely as Shapely
from pylowstroke.sketch_intersection_graph import IntersectionGraph
from pylowstroke.sketch_intersection_graph_extended import IntersectionGraph as ExtendedIntersectionGraph
from pylowstroke.sketch_hooks import cut_sketch_hooks
from pylowstroke.sketch_trimming import trimStraightStrokeCoordinatesDrawingField
from pylowstroke.sketch_vanishing_points import get_vanishing_points
from pylowstroke.sketch_camera import estimate_initial_camera_parameters
from ellipse import LsqEllipse


class StrokePoint:
	"""
	Class to handle a 2d point belonging to a stroke.
	A StrokePoint has 2d coordinates, adjacency information with other points
	and data attached to them in a dictionnary
	"""

	def __init__(self, x=0, y=0):
		"""
		:param x: x coordinate
		:param y: y coordinate
		"""
		self.set_coords(x, y)
		self.data = {}
		self.prev = None
		self.next = None

	def eq_coords(self, other):
		"""
		Check if other point has same coordinates

		:type other: StrokePoint
		:return: bool
		"""
		return all(self.coords == other.coords)

	def set_coords(self, x, y):
		"""
		set coordinates

		:param x: x coordinate
		:param y: y coordinate
		"""
		self.coords = np.array([x, y], dtype=float)

	def add_data(self, key, value):
		"""
		attach data to a point

		:type key: string
		:param value: anything
		"""
		self.data[key] = value

	def has_data(self, key):
		"""
		check if point has specific data

		:type key: string
		:return: bool
		"""
		return key in self.data.keys()

	def get_data(self, key):
		"""
		get specific data for this point,
		raises an exception if not found

		:type key: string
		:return: value = data[key]
		"""
		try:
			return self.data[key]
		except KeyError:
			#print("Error: no data named ", key, ' in point ', self)
			raise

	def get_distance(self, p):
		"""
		get distance to another point

		:type p: StrokePoint
		:return:
		"""
		return np.linalg.norm(self.coords - p.coords)

	def compute_speed(self):
		"""
		compute speed data based on timing data of adjacent points
		"""
		if self.prev == None and self.next == None:
			print("Warning: no adjacency for this point")
			length = 0
			time = 1
		elif self.prev == None:
			length = np.linalg.norm(self.next.coords - self.coords)
			time = self.next.data['time'] - self.data['time']
		elif self.next == None:
			length = np.linalg.norm(self.coords - self.prev.coords)
			time = self.data['time'] - self.prev.data['time']
		else:
			length = np.linalg.norm(self.coords - self.prev.coords)
			length += np.linalg.norm(self.next.coords - self.coords)
			time = self.next.data['time'] - self.prev.data['time']

		if np.isclose(time, 0.0):
			self.data['speed'] = -1.0
		else:
			self.data['speed'] = length / time

	def compute_curvature_circle(self, epsilon=1e-3):
		"""
		Compute curvature based on circle passing through neighbours

		:param epsilon: minimum distance between neighbours before they considered stacked
		"""
		if self.next == None or self.prev == None:
			curvature = 0
		else:
			A = self.prev.coords
			B = self.coords
			C = self.next.coords

			u1 = (A + B) / 2
			u2 = u1 + Tools.perp(B - A)

			v1 = (B + C) / 2
			v2 = v1 + Tools.perp(C - B)

			u = u2 - u1
			v = v2 - v1

			if np.linalg.norm(u) <= epsilon or np.linalg.norm(v) <= epsilon:
				curvature = 0
			elif abs(Tools.colinearity(u, v)) >= 1 - epsilon:
				curvature = 0
			else:
				Rc = Tools.get_intersection_2d(u1, u2, v1, v2)
				curvature = np.sign(Tools.colinearity(u, v)) / np.linalg.norm(Rc - A)

		self.data['curvature_circle'] = curvature

	def __str__(self):
		return "StrokePoint (%s, %s), data: %s" % (self.coords[0], self.coords[1], self.data)

class Stroke:
	"""
	A 2D stroke from a sketch
	A stroke is a mix a of different representations :
	- a list of points with data attached : cf StrokePoint
	- a Shapely LineString for basic geometry : cf ShapelyStroke
	"""

	def __init__(self, points, width=1.0,
				 do_fitting=False, do_compute=True,
				 sort_key=None, avail_keys={},
				 id=-1, acc_radius=5.0):
		"""
		create a 2D stroke from a list of points

		:param points: list of StrokePoint
		:param do_fitting: 
		:param do_compute: if set to true, compute points adjacency and properties
		:param sort_key: if set to a string in avail_keys, the corresponding key will be used to sort the points
		:param avail_keys: set of available data on points
		:param id: index of stroke in sketch
		:param acc_radius: accuracy radius for approximate geometry

		:type points: list
		:type width: float
		:type do_fitting: bool
		:type do_compute: bool
		:type sort_key: string
		:type avail_keys: set
		:type id: int
		:type acc_radius: float
		"""
		if avail_keys is None:
			avail_keys = set()
		self.points_list = points
		self.width = width
		self.avail_keys = set(avail_keys)
		self.linestring = Shapely.ShapelyStroke(self.points_list)

		if self.length() == 0.0:
			self.splits = [0.0, 1.0]
		else:
			self.splits = [self.linestring.project(p.coords) for p in self.points_list]

		#while self.splits[0] != 0.0:
		#	self.points_list.pop(0)
		#	self.linestring = Shapely.ShapelyStroke(self.points_list)
		#	self.splits = [self.linestring.project(p.coords) for p in self.points_list]
		#if not np.isclose(self.splits[-1], 0.0):
		#	# it's a circle, don't pull out the last segment
		#	while self.splits[-1] != 1.0:
		#		self.points_list.pop(-1)
		#		self.linestring = Shapely.ShapelyStroke(self.points_list)
		#		self.splits = [self.linestring.project(p.coords) for p in self.points_list]

		self.id = id
		if not hasattr(self, "acc_radius"):
			self.acc_radius = acc_radius
		if not hasattr(self, "axis_label"):
			self.axis_label = -1
		self.is_fitted_linear = False
		self.linear_fitting = []
		self.is_fitted_ellipse = False
		self.ellipse_fitting = []
		self._is_ellipse = None
		self.is_curved_method = "default"
		self.is_curved_threshold = -1.0

		if self.linestring.length() == 0:
			pass

		if sort_key != None and sort_key in self.avail_keys:
			self.sort_points(sort_key)

		if do_compute:
			self.compute_points_adjacency()
			self.compute_points_properties()

	def from_array(self, points):
		"""
		init this stroke from an array of 2d coordinates

		:param points: list of pairs of floats
		"""
		points_list = [StrokePoint(p[0], p[1]) for p in points]
		self.__init__(points_list, acc_radius=self.acc_radius)

	def __str__(self):
		return "Stroke: L=%s, nb points: %s, avail_data=%s" % (self.length(), len(self.points_list), self.avail_keys)

	####################################
	### Setters / getters			 ###
	####################################

	def set_width(self, w):
		self.width = w

	def has_data(self, key):
		return key in self.avail_keys

	def add_avail_data(self, key):
		self.avail_keys.add(key)

	def get_mean_data(self, key):
		if len(self.points_list) > 0 and not key in self.points_list[0].data:
			return 1.0
		return np.mean([p.get_data(key) for p in self.points_list])

	def length(self):
		"""

		:return: full length of polyline
		"""
		return self.linestring.length()

	def speed(self):
		"""
		compute global speed of stroke

		:return: s = l/dt
		"""
		if not self.has_data('time'):
			print("Error: no time data in stroke")
			raise KeyError

		if len(self.points_list) < 2:
			return 0
		else:
			times = [p.get_data('time') for p in self.points_list]
			d_t = max(times) - min(times)
			if np.isclose(d_t, 0.0):
				return -1.0
			return self.length() / d_t

	def set_id(self, id):
		self.id = id

	def get_id(self):
		return self.id

	def set_acc_radius(self, acc_radius):
		self.acc_radius = acc_radius

	def get_acc_radius(self):
		return self.acc_radius

	def set_is_curved_parameters(self, method, threshold):
		self.is_curved_method = method
		self.is_curved_threshold = threshold

	####################################
	### Points operations			 ###
	####################################

	def sort_points(self, key):
		"""
		sort points based on a specific data

		:type key: string
		"""
		if key not in self.avail_keys:
			print("Error: stroke do not have data : ", key)
			raise ValueError
		self.points_list = sorted(self.points_list, key=lambda p: p.get_data(key))

	def compute_points_adjacency(self):
		"""
		compute points adjacency

		"""
		prev = None
		for p in self.points_list:
			p.prev = prev
			if prev != None:
				prev.next = p
			prev = p

	def compute_points_properties(self):
		"""
		compute properties on points such as speed or curvature

		"""
		for p in self.points_list:
			p.compute_curvature_circle()
			if self.has_data('time'):
				p.compute_speed()

		self.avail_keys.add('curvature_circle')
		if self.has_data('time'):
			self.avail_keys.add('speed')

	def is_curved(self, threshold=0.01, method="pca"):
		"""
		Classify stroke between straight or curved.
		If pca is used then return ratio between svd on points coords.
		If length is used then return ratio between distance at extremities and full length
		:param threshold: the threshold to be used
		:param method: either "pca" or "length"
		:return: bool
		"""
		if np.isclose(self.length(), 0.0):
			return False
		if self.is_curved_method == "pca" and self.is_curved_threshold > 0.0:
			return self._get_curviness_with_pca() > self.is_curved_threshold
		elif self.is_curved_method == "length" and self.is_curved_threshold > 0.0:
			return self._get_curviness_with_length() > self.is_curved_threshold
		if self.is_curved_method == "pca_weighted":
			return self._get_curviness_with_pca_weighted()
		if method == "pca":
			return self._get_curviness_with_pca() > threshold
		elif method == "length":
			return self._get_curviness_with_length() > threshold
		else:
			print("Error: unkown method for computing curviness : %s" % method)
			raise NameError

	def _get_curviness_with_length(self):
		"""
		length method uses ratio betwwen distance between endpoints and total length

		:return: float
		"""
		start = self.points_list[0].coords
		end = self.points_list[-1].coords
		L = np.linalg.norm(end - start)
		ratio = 1 - (L / self.length())
		return ratio

	def _get_curviness_with_pca(self):
		"""
		PCA method uses the ratio between SV of SVD on points coords

		:return: float
		"""
		points = [[p.coords[0], p.coords[1]] for p in self.points_list]
		pca = PCA(n_components=2)
		svd = pca.fit(points).singular_values_
		ratio = (svd[1] / svd[0])
		return ratio

	def _get_curviness_with_pca_weighted(self):
		"""
		Eigenvalues method using a weighted scheme

		:return: float
		"""
		points = np.array([[p.coords[0], p.coords[1]] for p in self.points_list])
		x = points[:, 0]
		y = points[:, 1]
		#x = np.array([198.373606506687, 204.096013384136, 210.763861397859, 148.416555407210, 52.5447263017357])
		#y = np.array([227.907517752339, 230.566776027534, 232.270091785502, 203.644859813084, 155.164218958611])
		x_ = np.array([x[0]] + list(x) + [x[-1]])
		y_ = np.array([y[0]] + list(y) + [y[-1]])
		seg_lengths = np.sqrt((y_[:-1]- y_[1:])**2 + (x_[:-1]- x_[1:])**2)
		weights = seg_lengths[:-1] + seg_lengths[1:]
		mean_x = np.sum(weights*x)/np.sum(weights)
		mean_y = np.sum(weights*y)/np.sum(weights)
    
		zmx = (x-mean_x)
		zmy = (y-mean_y)

		D = np.array([[np.sum(zmx**2*weights), np.sum(zmx*zmy*weights)],
			[np.sum(zmx*zmy*weights), np.sum(zmy**2*weights)]])
		#print(x)
		#print(y)
		#print(x_)
		#print(y_)
		#print(seg_lengths)
		#print(weights)
		#print(mean_x)
		#print(mean_y)
		#print(zmx)
		#print(zmy)
		#print(D)
    
		v = np.linalg.eigvals(D)
		#w, v = np.linalg.eig(D)
		#print(v)
		#exit()
		#if (v[0, 0]/v[1, 1] > 1e-3):
		if np.min(v)/np.max(v) > 1e-3:
		#if (v[1]/v[0] > 1e-3):
			#print("True")
			return True
		#print("false")
		return False

	####################################
	### Different kinds of fitting	 ###
	####################################

	def get_ellipse_fitting(self, epsilon=1.0):
		"""

		:param epsilon:
		:return:
		"""
		if self.is_fitted_ellipse:
			return self.ellipse_fitting

		x = np.array([p.coords[0] for p in self.points_list])
		y = np.array([p.coords[1] for p in self.points_list])

		xmean, ymean = x.mean(), y.mean()
		x -= xmean
		y -= ymean
		U, S, V = np.linalg.svd(np.stack((x, y)))
		N = x.shape[0]

		tt = np.linspace(0, 2 * np.pi, 1000)
		circle = np.stack((np.cos(tt), np.sin(tt)))  # unit circle
		transform = np.sqrt(2.0 / float(N)) * np.matmul(U, np.diag(S))  # transformation matrix

		fit = transform.dot(circle) + np.array([[xmean], [ymean]])
		fit = np.transpose(fit)
		fit = rdp(fit, epsilon=epsilon)
		if len(x) > 5:
			x += xmean
			y += ymean
			try:
				ellipse_fit = LsqEllipse().fit(np.c_[x, y])
				if len(ellipse_fit.coefficients) > 0:
					fit = ellipse_fit.return_fit(n_points=20)
			except:
				pass

		self.is_fitted_ellipse = True
		self.ellipse_fitting = [fit, U, S]
		#if self.is_ellipse():
		#	print(self.id)
		#	print("x", x)
		#	print("y", y)
		#	print(U, S, V)
		#	print(fitting)
		return fit, U, S

	def is_ellipse(self):
		"""

		:return:
		"""
		if not "_is_ellipse" in dir(self):
			self._is_ellipse = None
		if self.is_fitted_ellipse and self._is_ellipse is not None:
			return self._is_ellipse
		if len(self.points_list) < 5:
			self._is_ellipse = False
			return False
		ellipse, u, s = self.get_ellipse_fitting()

		# check scaling to detect a line
		if np.isclose(s[0], 0.0) or np.isclose(s[1], 0.0):
			self._is_ellipse = False
			return False
		if s[0] / s[1] > 10.0 or s[1] / s[0] > 10.0:
			self._is_ellipse = False
			return False

		poly = self.linestring.linestring.buffer(self.get_acc_radius())
		if len(list(poly.interiors)) == 0:
			self._is_ellipse = False
			return False
		self._is_ellipse = True
		return True

	# Linear :
	def get_linear_fitting(self):
		"""
		Fit a line M, V such that P = f(M, V) = M + lambda * V
		using a linear regression

		:return: M, V
		"""
		if self.length() == 0:
			print("Warning: skipping stroke linear regression of length 0.")
		if self.is_fitted_linear:
			return self.linear_fitting

		regr = linear_model.LinearRegression()
		# use ay + b = x instead of ax + b = y if the line is more vertical than
		# horizontal
		use_inverse_model = False
		X = np.array([p.coords[0] for p in self.points_list]).reshape(-1, 1)
		Y = np.array([p.coords[1] for p in self.points_list])
		if abs(np.max(X.flatten()) - np.min(X.flatten())) < \
				abs(np.max(Y.flatten() - np.min(Y.flatten()))):
			use_inverse_model = True
			X = np.array([p.coords[1] for p in self.points_list]).reshape(-1, 1)
			Y = np.array([p.coords[0] for p in self.points_list])
		regr.fit(X, Y)
		# M = np.array([self.points_list[0].coords[0], regr.predict([[self.points_list[0].coords[0]]])[0]])
		M = np.array([X[0][0], regr.predict([X[0]])[0]])
		V = np.array([1, regr.coef_[0]])
		if use_inverse_model:
			M = M[[1, 0]]
			V = np.array([regr.coef_[0], 1])
		else:
			V = np.array([1, regr.coef_[0]])
		V /= np.linalg.norm(V)
		self.is_fitted_linear = True
		self.linear_fitting = [M, V]
		return M, V

	def get_segment_fitting(self):
		"""
		fit this stroke to a line segment [A,B]

		:return: [A, B]
		"""
		P, V = self.get_linear_fitting()
		Q = P + V
		A = Tools.project_point_on_segment(P, Q, self.points_list[0].coords)
		B = Tools.project_point_on_segment(P, Q, self.points_list[-1].coords)
		return [A, B]

	def get_segment_fitting_from_points(self):
		"""
		fit this stroke to a line segment [A,B]

		:return: [A, B]
		"""
		P, V = self.get_linear_fitting()
		Q = P + V

		A, ta = None, 1e99
		B, tb = None, -1e99
		for p in self.points_list:
			X, tx = Tools.project_point_on_segment(P, Q, self.points_list[0].coords, out_r=True)
			if tx < ta:
				A, ta = X, tx
			if tx > tb:
				B, tb = X, tx

		return [A, B]

	def resample_ideal_line(self):
		P, V = self.get_linear_fitting()
		Q = P + V
		points_dot_r = [Tools.project_point_on_segment(P, Q, p.coords, out_r=True)[1]
						for p in self.points_list]
		A = Tools.project_point_on_segment(P, Q, self.points_list[np.argmin(points_dot_r)].coords)
		B = Tools.project_point_on_segment(P, Q, self.points_list[np.argmax(points_dot_r)].coords)
		return [A, B]

	####################################
	### Evaluation					 ###
	####################################


	def eval_point(self, t):
		"""
		Evaluation of the stroke on a given parameter in [0;1]
		returns a point with interpolated data

		:type t: float
		:return: StrokePoint
		"""
		if t == 1.0:
			# todo copy instead of link
			return self.points_list[-1]

		x, y = self.eval_polyline(t)
		out = StrokePoint(x, y)

		n = next(i for i, v in enumerate(self.splits) if v > t)
		sub_t = (t - self.splits[n - 1]) / (self.splits[n] - self.splits[n - 1])

		out.prev = self.points_list[n - 1]
		out.next = self.points_list[n]

		for k in self.avail_keys:
			out.add_data(k, out.prev.data[k] + sub_t * (out.next.data[k] - out.prev.data[k]))

		return out


	def eval_polyline(self, t):
		"""
		eval stroke at parameter t in [0;1]
		returns 2d coordinates of the point on the polyline

		:type t: float
		:return: (x,y)
		"""
		return self.linestring.eval(t)


	####################################
	### Polyline operations			 ###
	####################################

	def set_points_list(self, points, do_compute=True):
		"""
		overwrites current polyline with a new points list.
		updates  polyline

		:param points: list of (x,y)
		:param do_compute: flag to recompute basic stuff
		"""
		self.points_list = points

		del self.linestring
		self.linestring = Shapely.ShapelyStroke(points)
		self.splits = [self.linestring.project(p.coords) for p in self.points_list]

		self.compute_points_adjacency()

		if do_compute:
			self.compute_points_properties()

	def simplify(self, tolerance):
		"""
		Destructively simplify stroke using shapely simplification (overwrites current polyline)

		:type tolerance: float
		"""
		new_s = self.linestring.get_simplification(tolerance)
		new_p = [self.eval_point(self.linestring.project(c)) for c in list(new_s.coords)]
		self.set_points_list(new_p, do_compute=True)

	def resample_rdp(self, epsilon, do_compute=True):
		"""
		destructively resample this stroke using Douglas Peuckler

		:type epsilon: float
		:param do_compute: flag to compute basic data
		"""
		points = [p.coords for p in self.points_list]
		mask = rdp(points, epsilon=epsilon, return_mask=True, algo="iter")
		points_list = [self.points_list[k] for k in range(len(self.points_list)) if mask[k]]
		self.set_points_list(points_list, do_compute=do_compute)

	def resample_linestring(self, dist, do_compute=True, policy=lambda n: np.floor(n)):
		"""
		destructively resample this linestring with uniform distance dist between points

		:type dist: float
		:param do_compute: flag to compute basic data
		:param policy: policy used to coompute nb of points, default: floor
		"""
		N = int(policy(self.length() / dist))

		if N < 1:
			# print("Warning: resampling aborted because inter-point distance is too high.")
			return

		points = [self.eval_point(t) for t in np.linspace(0, 1, N + 1)]
		self.set_points_list(points, do_compute=do_compute)

	####################################
	### fitting operations			 ###
	####################################

	#def remove_hooks(self):
	#	"""
	#	Remove hooks on this stroke using method described in Lifting3D.
	#	We will first cut the strokes and then simply keep the longest part
	#	for each stroke. Other heuristics could be used.

	#	"""

	####################################
	### Binary operators			 ###
	####################################

	def intersects(self, stroke):
		return self.linestring.intersects(stroke.linestring)

	def get_intersection(self, stroke):
		return self.linestring.get_intersection(stroke.linestring)

	def get_intersection_parameters(self, stroke):
		return self.linestring.get_intersection_parameter(stroke.linestring)

	def intersects_fuzzy(self, stroke, buffer):
		return self.linestring.intersects_buffer(stroke.linestring, buffer)

	def get_intersection_fuzzy(self, stroke, buffer):
		return self.linestring.get_intersection_buffer(stroke.linestring, buffer)

	def get_intersection_parameters_fuzzy(self, stroke, buffer):
		return self.linestring.get_intersection_parameter_buffer(stroke.linestring, buffer)

	def get_distance(self, p):
		"""
		distance of a point to this polyline

		:param p: 2d point
		:return: float
		"""
		return self.linestring.get_distance(p.coords)

	def get_line_angle(self, stroke):
		"""
		Binary angle score between 2 strokes

		:type stroke: StrokePoint
		:return: float
		"""
		# if self.is_curved() or stroke.is_curved():
		#	print("Warning: getting straight angle of curved strokes.")

		_, u1 = self.get_linear_fitting()
		_, u2 = stroke.get_linear_fitting()

		return acos(min(1.0, abs(np.dot(u1, u2))))

	def get_intersection_angles(self, stroke, intersection=[]):
		"""
		Get angles of intersections with another stroke

		:type stroke: StrokePoint
		:param intersection: optional parameter in case intersection has been generated by calling get_intersection_fuzzy
		:return: list of angles
		"""
		P = intersection
		if len(P) == 0:
			P = self.get_intersection(stroke)
		out = []

		for p in P:
			t1 = self.linestring.project(p)
			t2 = stroke.linestring.project(p)

			# corner case
			if np.isclose(t1, 1.0):
				hi = len(self.splits) - 1
				low = len(self.splits) - 2

			for i, spl in enumerate(self.splits):
				if spl > t1:
					hi, low = i, i - 1
					break
			u1 = self.points_list[hi].coords - self.points_list[low].coords
			u1 /= np.linalg.norm(u1)

			# corner case
			if np.isclose(t2, 1.0):
				hi = len(stroke.splits) - 1
				low = len(stroke.splits) - 2
			for i, spl in enumerate(stroke.splits):
				if spl > t2:
					hi, low = i, i - 1
					break
			u2 = stroke.points_list[hi].coords - stroke.points_list[low].coords
			u2 /= np.linalg.norm(u2)

			out.append(acos(abs(np.dot(u1, u2))))

		return out

	####################################
	### Display						 ###
	####################################

	def get_line_display_data(self, color_data, linewidth_data):
		segs = []
		lws = []
		col = []
		for i in range(len(self.points_list) - 1):
			p1 = self.points_list[i]
			p2 = self.points_list[i + 1]
			segs.append([p1.coords, p2.coords])

			if color_data != None:
				c = np.mean([color_data(p1), color_data(p2)])
				col.append(c)

			if linewidth_data != None:
				try:
					l = np.mean([linewidth_data(p1), linewidth_data(p2)])
				except KeyError:
					l = 1.0
				lws.append(l)

		return (segs, lws, col)

	# slow plot
	def plot_dirty(self, color=None, lw=lambda s: None):
		for i, p in enumerate(self.points_list):
			point = p.coords
			if i != 0:
				if color == None:
					plt.plot([prev[0], point[0]], [prev[1], point[1]],
							 linewidth=self.width if lw(self) == None else lw(self))
				else:
					plt.plot([prev[0], point[0]], [prev[1], point[1]], color=color,
							 linewidth=self.width if lw(self) == None else lw(self))
			prev = point

	def remove_zero_length_edges(self):
		remove_points = []
		p_id = 0
		while p_id < len(self.points_list)-1:
			if np.isclose(np.linalg.norm(self.points_list[p_id].coords -
										 self.points_list[p_id+1].coords), 0.0):
				remove_points.append(p_id+1)
				p_id += 1
			p_id += 1
		for del_id in reversed(remove_points):
			del self.points_list[del_id]

class Sketch:
	"""
	a Sketch is a list of strokes
	it has width, height and pen_width attributes

	"""

	def __init__(self, strokes=None):
		self.width = 1000
		self.height = 1000
		self.pen_width = 0
		if strokes is None:
			self.strokes = []
		else:
			self.strokes = strokes
		self.intersection_graph = IntersectionGraph()
		self.extended_intersection_graph = ExtendedIntersectionGraph()

	####################################
	### Global operations			 ###
	####################################

	def set_pen_width(self, pw):
		self.pen_width = pw
		for st in self.strokes:
			st.width = pw

	def compute_points_adjacency(self):
		for st in self.strokes:
			st.compute_points_adjacency()

	def compute_points_properties(self):
		for st in self.strokes:
			st.compute_points_properties()

	def resample_rdp(self, epsilon, do_compute=False):
		for st in self.strokes:
			st.resample_rdp(epsilon, do_compute)

	def resample_linestring(self, dist, do_compute=False, policy=lambda n: np.floor(n)):
		for st in self.strokes:
			st.resample_linestring(dist, do_compute, policy)

	def simplify_strokes(self, tolerance):
		for st in self.strokes:
			st.simplify(tolerance)

	def remove_hooks(self):
		cut_sketch_hooks(self)
		original_ids = {}
		del_ids = []
		for s_id, s in enumerate(self.strokes):
			org_ids = tuple(s.original_id)
			if org_ids in original_ids.keys():
				original_ids[org_ids].append(s_id)
			else:
				original_ids[org_ids] = [s_id]
		for s_ids in original_ids.values():
			if len(s_ids) == 1:
				continue

		    # choose biggest_part
			max_len_id = np.argmax([self.strokes[s_id].length() for s_id in s_ids])

		    # choose first seg (lifting3d strategy)
			#max_len_id = 0

			for i, del_id in enumerate(s_ids):
				if i != max_len_id:
					del_ids.append(del_id)
		for del_id in np.flip(np.sort(del_ids)):
			del self.strokes[del_id]

	
	def trim_strokes(self):
		for s_id, s in enumerate(self.strokes):
			if len(s.points_list) < 2:
				continue
			trimStraightStrokeCoordinatesDrawingField(s, self.height)

	def get_vanishing_points(self):
		vps, p, failed = get_vanishing_points(self)
		return vps, p, failed

	def estimate_camera(self):

		vps, p, failed = self.get_vanishing_points()
		cam_param, lines_group, vps, vp_new_ind = estimate_initial_camera_parameters(vps, p, self)
		return cam_param, lines_group

	def shave_empty_strokes(self, maxlen=None):
		"""
		remove empty or short strokes

		:type maxlen: float
		:param maxlen: length threshold
		"""
		for st in reversed(self.strokes):
			if maxlen == None:
				maxlen = st.get_acc_radius()
			if st.length() <= maxlen:
				self.strokes.remove(st)
		self.update_stroke_indices()

	def set_is_curved_parameters(self, method, threshold):
		for s_id in range(len(self.strokes)):
			self.strokes[s_id].set_is_curved_parameters(method, threshold)

	def update_stroke_indices(self):
		for s_id, s in enumerate(self.strokes):
			s.set_id(s_id)

	def compute_intersection_graph(self, extended=False):
		if extended:
			self.extended_intersection_graph.init_intersection_graph(self.strokes)
			self.intersection_graph = self.extended_intersection_graph
		else:
			self.intersection_graph.init_intersection_graph(self.strokes)

	####################################
	### Display						 ###
	####################################

	def display_strokes(self,
						fig=None, ax=None,
						color_data=lambda p: p.get_data("curvature_circle"),
						color_process=lambda C: [np.mean([abs(c) for c in C])],
						linewidth_data=lambda p: p.get_data("pressure"),
						linewidth_process=None,
						cmap=pl.cm.jet,
						norm_global=True):
		"""
		each sub_stroke normalized in sketch
		process color_data afterwise, e.g mean all sub_strokes
		todo display_strokes_2(color_data=lambda s:s.is_curved())

		:param fig: plt figure
		:param ax:  plt axis
		:param color_data: lambda on each point to specify data used for color
		:param color_process: lambda
		:param linewidth_data:
		:param linewidth_process:
		:param cmap:
		:param norm_global:
		"""
		if fig == None or ax == None:
			fig, ax = plt.subplots()
			ax.set_xlim(0, self.width)
			ax.set_ylim(self.height, 0)

		mn = np.inf
		mx = -np.inf

		display_data = []
		for s in self.strokes:
			(seg, lws, col) = s.get_line_display_data(color_data, linewidth_data)
			if color_process != None: col = color_process(col)
			if linewidth_data == None: lws = [s.width]
			if linewidth_process != None: lws = linewidth_process(lws)
			colmin = np.min(col)
			colmax = np.max(col)
			if colmin < mn: mn = colmin
			if colmax > mx: mx = colmax
			display_data.append((seg, lws, col))

		for d in display_data:
			if norm_global:
				col_data = Tools.normalize_list(d[2], mn, mx)
			else:
				col_data = Tools.normalize_list(d[2])
			colors = cmap(col_data)
			lc = LineCollection(d[0], linewidths=d[1], colors=colors)
			ax.add_collection(lc)

	def display_strokes_2(self,
						  fig=None, ax=None,
						  color_process=lambda s: "red" if s.is_curved() else "blue",
						  linewidth_data=lambda p: p.get_data("pressure")+0.5,
						  linewidth_process=None,
						  use_cmap=False,
						  cmap=pl.cm.jet,
						  norm_global=True,
						  # display_interval is useful when we want to partially
						  # plot the sketch
						  display_strokes=[]):
		"""
		process color based on stroke properties
		"""
		if fig == None or ax == None:
			fig, ax = plt.subplots()
			ax.set_xlim(0, self.width)
			ax.set_ylim(self.height, 0)
		mn = np.inf
		mx = -np.inf

		display_data = []
		for s in self.strokes:
			(seg, lws, col) = s.get_line_display_data(None, linewidth_data)
			if color_process != None: col = color_process(s)
			if linewidth_data == None: lws = [s.width]
			if linewidth_process != None: lws = linewidth_process(lws)
			if use_cmap:
				colmin = np.min(col)
				colmax = np.max(col)
				if colmin < mn: mn = colmin
				if colmax > mx: mx = colmax
			display_data.append((seg, lws, col))

		if use_cmap:
			# normalize over all strokes
			colors = [d[2] for d in display_data]
			if norm_global:
				col_data = Tools.normalize_list(colors, mn, mx)
			else:
				col_data = Tools.normalize_list(colors)
			colors = cmap(col_data)
			# get color data back in display_data
			display_data = [(d[0], d[1], colors[d_id]) for d_id, d in enumerate(display_data)]

		if len(display_strokes) == 0:
			display_strokes = list(range(len(self.strokes)))
		for d_stroke_id in display_strokes:
			d = display_data[d_stroke_id]
			col = d[2]
			#if self.strokes[0].points_list[0].has_data("pressure"):
			#	col = mcolors.to_rgb(col)
			#	col = [(col[0], col[1], col[2], min(1.0, 0.3+p.get_data("pressure"))) for p in self.strokes[d_stroke_id].points_list]
			lc = LineCollection(d[0], linewidths=lws, colors=col, antialiaseds=True)
			ax.add_collection(lc)

	def __str__(self):
		strokes = ""
		for s in self.strokes: strokes += str(s) + "\n"
		return "Sketch2D : width=%s, height=%s, pen_width=%s, strokes : \n%s" % (
			self.width, self.height, self.pen_width, strokes)