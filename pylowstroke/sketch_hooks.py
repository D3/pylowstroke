import numpy as np
from copy import deepcopy
import os
from shapely.geometry import LineString
from sklearn.neighbors import KDTree
from scipy.linalg import svd

import seaborn as sns
cmap = sns.color_palette("Dark2", 8).as_hex()

def cut_sketch_hooks(sketch, VERBOSE=False):
    #new_sketch = deepcopy(sketch)
    #new_sketch.strokes = []
    new_strokes = []
    if VERBOSE:
        os.system("rm -rf "+os.path.join(sketch.sketch_folder, "python_indices.txt"))

    for s_i, s in enumerate(sketch.strokes):
        if len(s.points_list) < 2:
            vertices = np.array([p.coords for p in s.points_list])
            indices_sampled = list(np.arange(len(vertices)))
        else:
            s.resample_rdp(epsilon=0.5)
            points = np.array(s.linestring.linestring.coords)
            #print("points", points)
            #print(np.array(s.linestring.linestring))
            vertices, indices_sampled = densely_resample(points)
        #print(s_i, len(vertices), len(s.points_list))
        if len(vertices) == 0:
            continue
        #print("point check", vertices[indices_sampled])

        if len(vertices) < 3:
            indices_set = [indices_sampled]
        else:
            indices_set = cut_stroke(vertices, np.arange(0, len(s.points_list)), indices_sampled)
        #print(s_i)
        #print("indices_set", indices_set)
        if VERBOSE:
            for ind in indices_set:
                with open(os.path.join(sketch.sketch_folder, "python_indices.txt"), "a") as fp:
                    fp.write(",".join([str(s) for s in list(np.array(ind)+1)])+"\n")
        #for ind in indices_set:
        #    print(list(np.array(ind)+1))

        # create new strokes
        for i, indis in enumerate(indices_set):

            # lifting3d bug
            #indis = indices_set[0]

            new_s = deepcopy(s)
            # good copy
            new_s.from_array(points[indis])
            if s.has_data("pressure"):
                for new_p_id, p_id in enumerate(indis):
                    if s.points_list[p_id].has_data("pressure"):
                        new_s.points_list[new_p_id].add_data("pressure",
                            s.points_list[p_id].get_data("pressure"))
                new_s.add_avail_data("pressure")
            if s.has_data("time"):
                for new_p_id, p_id in enumerate(indis):
                    if s.points_list[p_id].has_data("time"):
                        new_s.points_list[new_p_id].add_data("time",
                            s.points_list[p_id].get_data("time"))
                new_s.add_avail_data("time")
            color = "#000000"
            if len(indices_set) > 1:
                color = cmap[i%8]
            new_s.svg_color = color
            new_strokes.append(new_s)
            if hasattr(s, "original_id"):
                new_s.original_id = deepcopy(s.original_id)
            else:
                new_s.original_id = [s_i]
    sketch.strokes = new_strokes

def cut_stroke(points_sampled, indices, indices_sampled):
    k, g0 = polyline_curvature_circle_fit(points_sampled, indices_sampled)
    #print("k", k)
    #print("g0", g0)
    indices_set = []

    #print(g0)
    if len(g0) > 0:
        max_k_ind = g0[0]
    else:
        # find max_curvature
        curvatures_sampled = np.zeros(len(points_sampled))
        for k_i, i in enumerate(indices_sampled[:-1]):
            #print(k_i, i)
            curvatures_sampled[indices_sampled[k_i]:indices_sampled[k_i+1]] = 0.5*(k[k_i]+k[k_i+1])
        #print(indices_sampled)
        #print(len(curvatures_sampled), curvatures_sampled[indices_sampled[-1]:])
        curvatures_sampled[indices_sampled[-1]:] = k[-1]
        #print(curvatures_sampled)
        median_curvature = np.median(curvatures_sampled)
        ind_cut = np.argwhere(np.logical_and(k > 10.0*median_curvature, k > 0.125))
        #print(points_sampled)
        #print("ind_cut", ind_cut)
        
        # Cut
        if len(ind_cut) == 0:
            # check beginning angles
            v1 = points_sampled[0] - points_sampled[1]
            v2 = points_sampled[2] - points_sampled[1]
            #print(v1, v2)
            v1 /= np.linalg.norm(v1)
            v2 /= np.linalg.norm(v2)
            #print(points_sampled[:3])
            #print(v1, v2)
            #print("dot prod", np.dot(v1, v2))
            if np.dot(v1, v2) > 0:
                #cut
                indices_set = [indices[1:]]
            else:
                indices_set = [indices]
            return indices_set
        else:
            cut_candidates = k[ind_cut]
            #print("cut_candidates", cut_candidates)
            max_k_ind = ind_cut[cut_candidates == np.max(cut_candidates)]
            max_k_ind = max_k_ind[0]
            #print("max_k_ind", max_k_ind)
            # left and right strokes
            if max_k_ind == 0:
                max_k_ind = 1
            if max_k_ind == len(indices_sampled)-1:
                max_k_ind -= 1
    
    left_indices_sampled = indices_sampled[0:max_k_ind+1]
    left_indices = indices[0:max_k_ind+1]
    #print("max_k_ind", max_k_ind, indices_sampled)
    right_indices_sampled = list(np.array(indices_sampled[max_k_ind:]) - indices_sampled[max_k_ind])
    right_indices = indices[max_k_ind:]
    #print("indices", indices)
    #print(left_indices_sampled)
    #print(left_indices)
    #print(right_indices_sampled)
    #print(right_indices)
     
    max_k_ind = indices_sampled[max_k_ind]
    
    
    left_points_sampled   = points_sampled[:max_k_ind+1]
  
    right_points_sampled  = points_sampled[max_k_ind:]
  
    
    # Check if a hook:

    l_left_stroke = LineString(left_points_sampled).length
    l_stroke = LineString(points_sampled).length
    l_right_stroke = LineString(right_points_sampled).length
    
    if (l_left_stroke > 0.15*l_stroke):
        #print("left branch")
        #print(left_points_sampled)
        left_indices_set = cut_stroke(left_points_sampled, left_indices, left_indices_sampled)
        
    else:
        #hook
        left_indices_set = []
    
    if (l_right_stroke > 0.15*l_stroke):
        #print("right branch")
        right_indices_set = cut_stroke(right_points_sampled, right_indices, right_indices_sampled)
    else:
        #hook
        right_indices_set = []
    
    if len(list(left_indices_set)) > 0:
        #indices_set.append(list(left_indices_set[0]))
        indices_set += left_indices_set
    if len(list(right_indices_set)) > 0:
        #indices_set.append(list(right_indices_set[0]))
        indices_set += right_indices_set
    return indices_set


def polyline_curvature_circle_fit(points, indices_sampled):
    dist_pix = 6
    g0_points = []
    tree = KDTree(points, leaf_size=2)
    ids = tree.query_radius(points, r=dist_pix)
    curvatures = np.zeros(len(indices_sampled))
    #print(ids)
    for flattened_p_id, p_id in enumerate(indices_sampled):
        #print(p_id)
        ids[p_id] = range(np.min(ids[p_id]), np.max(ids[p_id])+1)
        if len(ids[p_id]) < 4:
            curvatures[flattened_p_id] = 0.0
            sigma = 0.0
        else:
            #print("ids[p_id]", ids[p_id], len(points))
            #print("circle_fit", points[ids[p_id]])
            par, is_singular = circle_fit_pratt(points[ids[p_id]])
            sigma = par[3]
            if not is_singular:
                curvatures[flattened_p_id] = 1.0/par[2]
        #print(curvatures[p_id], sigma, ids[p_id])
        if np.isnan(curvatures[flattened_p_id]):
            curvatures[flattened_p_id] = 0.0
        
        #print("sigma", sigma)
        #print(ids[p_id], p_id)
        # check for g0 discontinuity
        if sigma > 0.5 and p_id > np.min(ids[p_id]) and p_id < np.max(ids[p_id]):
            sorted_ids = np.sort(ids[p_id])
            #print(sorted_ids)
            ids_left = sorted_ids[sorted_ids <= p_id]
            ids_right = sorted_ids[sorted_ids >= p_id]
            #print(ids_left, ids_right)
            sigma_l = 0.0
            sigma_r = 0.0
            if len(ids_left) > 3:
                par, is_singular = circle_fit_pratt(points[ids_left])
                #print(par)
                sigma_l = par[3]
            if len(ids_right) > 3:
                par, is_singular = circle_fit_pratt(points[ids_right])
                sigma_r = par[3]
                #print(par)
            if sigma_l < 0.5 and sigma_r < 0.5:
                g0_points.append(flattened_p_id)


    return curvatures, g0_points

def convert_input(coords):
    """
    Converts the input coordinates from a 2D List or 2D np.ndarray to 2 separate 1D np.ndarrays.
    Parameters
    ----------
    coords: 2D List or 2D np.ndarray of shape (n,2). X,Y point coordinates.
    Returns
    -------
    x   : np.ndarray. X point coordinates.
    y   : np.ndarray. Y point coordinates.
    """
    if isinstance(coords, np.ndarray):
        assert coords.ndim == 2, "'coords' must be a (n, 2) array"
        assert coords.shape[1] == 2, "'coords' must be a (n, 2) array"
        x = coords[:, 0]
        y = coords[:, 1]
    elif isinstance(coords, list):
        x = np.array([point[0] for point in coords])
        y = np.array([point[1] for point in coords])
    else:
        raise Exception("Parameter 'coords' is an unsupported type: " + str(type(coords)))
    return x, y

def center_data(x, y):
    """
    Computes the centroid of points (x, y) and subtracts the centroid from the points to center them.
    Parameters
    ----------
    x   : np.ndarray. X point coordinates.
    y   : np.ndarray. Y point coordinates.
    Returns
    -------
    xc      : np.ndarray. x points centered on the computed centroid.
    yc      : np.ndarray. y points centered on the computed centroid.
    centroid: np.ndarray. [x, y] centers.
    """
    centroid = np.array([x.mean(), y.mean()])
    xc = x - centroid[0]
    yc = y - centroid[1]
    return xc, yc, centroid

def sigma(x, y, xc, yc, r):
    """
    Computes the sigma (RMS error) of a circle fit (xc, yc, r) to a set of 2D points (x, y).
    ----------
    x   : np.ndarray. X point coordinates.
    y   : np.ndarray. Y point coordinates.
    xc  : float. Circle center X coordinate.
    yc  : float. Circle center Y coordinate.
    r   : float. Circle radius.
    Returns
    -------
    sigma : float. Root Mean Square of error (distance) between points (x, y) and circle (xc, yc, r).
    """
    dx = x - xc
    dy = y - yc
    s: float = np.sqrt(np.mean((np.sqrt(dx ** 2 + dy ** 2) - r) ** 2))
    #s: float = np.mean(np.abs(np.sqrt(dx ** 2 + dy ** 2) - r))
    return s

def circle_fit_pratt(coords):
    """
    Based on original MATLAB code by Nikolai Chernov: https://people.cas.uab.edu/~mosya/cl/PrattSVD.m.
    Algebraic circle fit by Pratt
    V. Pratt, "Direct least-squares fitting of algebraic surfaces",
    Computer Graphics, Vol. 21, pages 145-152 (1987)
    Parameters
    ----------
    coords: 2D List or 2D np.ndarray of shape (n,2). X,Y point coordinates.
    Returns
    -------
    xc  : float. x coordinate of the circle fit
    yc  : float. y coordinate of the circle fit
    r   : float. Radius of the circle fit
    s   : float. Sigma (RMS of error) of the circle fit
    """
    is_singular = False
    x, y = convert_input(coords)
    X, Y, centroid = center_data(x, y)
    Z = X * X + Y * Y
    ZXY1 = np.array([Z, X, Y, np.ones(len(Z))]).transpose()
    U, S, V = svd(ZXY1, full_matrices=False)
    S = np.diag(S)
    V = V.transpose()
    if S[3, 3] / S[0, 0] < 1e-12:  # singular case
        is_singular = True
        A = V[:, 3]
    else:
        W = np.matmul(V, S)
        Binv = np.array([[0, 0, 0, -0.5], [0, 1, 0, 0], [0, 0, 1, 0], [-0.5, 0, 0, 0]])
        D, E = np.linalg.eig(np.matmul(np.matmul(np.transpose(W), Binv), W))
        sorter = np.argsort(D)
        A = E[:, sorter[1]]
        for i in range(4):
            S[i, i] = 1 / S[i, i]
        A = np.matmul(np.matmul(V, S), A)
    #print(A[0])
    if A[0] == 0.0:
        c = [np.inf, np.inf]
    else:
        c = -(A[1:3]).transpose() / A[0] / 2 + centroid
    #print(c)
    xc = c[0]
    yc = c[1]
    if A[0] == 0.0:
        r = np.inf
        tt = -np.inf
        s = 0.0
    else:
        r = np.sqrt(A[1] ** 2 + A[2] ** 2 - 4 * A[0] * A[3]) / abs(A[0]) / 2
        tt = np.sqrt((x - xc)**2 +  (y - yc)**2) - r
        s = np.sum(np.abs(tt))/len(tt)
    if is_singular:
        s = 0.0

    #s = sigma(x, y, xc, yc, r)
    return [xc, yc, r, s], is_singular

def densely_resample(vertices, resample_dist=1.0):
    vertices = np.array(vertices)
    ind_orignal = []
    vertices_new = []
    for i in range(len(vertices)-1):
        ind_orignal.append(len(vertices_new))
        dir = vertices[i+1] - vertices[i]
        dist = np.linalg.norm(dir)
        k = np.ceil(dist/resample_dist)
        #vertices_sampled = np.dot(np.arange(1, k)/k, dir)
        vertices_sampled = []
        for j in range(int(k)):
            vertices_sampled.append(vertices[i]+j/k*dir)
        
        #vertices_sampled = vertices[i-1] + [[0:k-1]'.*dir(1,1) [0:k-1]'.*dir(1,2)]/k;
        
        vertices_new += vertices_sampled

    ind_orignal.append(len(vertices_new))
    vertices_new.append(vertices[-1])

    return np.array(vertices_new), ind_orignal
