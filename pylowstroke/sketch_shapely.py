from shapely.geometry import LineString, Point, MultiPoint, MultiLineString
from shapely.errors import TopologicalError
import shapely.ops as ops
import numpy as np
import math

class ShapelyStroke:
	"""
	Class to represent a stroke as a polyline using Shapely
	"""
	def __init__(self, points, is_array=False):
		"""
		Create a stroke from a list of StrokePoints

		:param points: list of points
		:param is_array: if set to true, points is expected to be a list of 2d coordinates
		"""
		if is_array:
			self.linestring = LineString(points)
		else:
			self.linestring = LineString([p.coords for p in points])

	def length(self):
		"""
		Compute full length of polyline

		:return: length
		"""
		return self.linestring.length

	def eval(self, t):
		"""
		Eval polyline at parameter t

		:param t: parameter in [0 -> 1]
		:return: (x, y)
		"""
		if t > 1.0 or t < 0.0:
			print("Error: cannot evaluate stroke at t=%s" % t)
			raise ValueError

		p = self.linestring.interpolate(t, normalized=True)
		return np.array([p.x, p.y])

	def eval_tangent(self, t):
		"""
		Evaluate the tangent of the polyline at given parameter

		:param t: parameter between 0 and 1
		:return: tangeant at t
		"""
		p = self.eval(t)
		points_coords = np.array(self.linestring.coords)
		points_distances = np.linalg.norm(points_coords - p, axis=1)
		closest_p_id = np.argmin(points_distances)
		if closest_p_id == 0:
			tan_vec = points_coords[1] - points_coords[0]
		elif closest_p_id == len(points_coords) - 1:
			tan_vec = points_coords[-1] - points_coords[-2]
		else:
			tan_vec = points_coords[closest_p_id+1] - points_coords[closest_p_id-1]
		tan_norm = np.linalg.norm(tan_vec)
		if not np.isclose(tan_norm, 0.0):
			tan_vec /= np.linalg.norm(tan_vec)

		return tan_vec

	def union(self, h_stroke):
		"""
		Concatenate 2 polylines

		:param h_stroke: the other ShapelyStroke
		:return: a new ShapelyStroke
		"""
		u = self.linestring.union(h_stroke.linestring)
		L = list(ops.linemerge(u).coords)
		return ShapelyStroke(L, is_array=True)

	def add_seg(self, S):
		"""
		Add a segment to a polyline

		:param S: the segment to add
		"""
		if len(self.linestring.coords) == 0:
			self.linestring = LineString(S)
		else:
			u = self.linestring.union(LineString(S))
			if isinstance(u, MultiLineString):
				outcoords = [list(i.coords) for i in u]
				self.linestring = LineString([i for sublist in outcoords for i in sublist])
			else:
				L = list(ops.linemerge(u).coords)
				self.linestring = LineString(L)

	def get_distance(self, p):
		"""
		Compute minimal distance to a point

		:param p: (x, y)
		:return: float
		"""
		return Point(p).distance(self.linestring)

	def project(self, p):
		"""
		Project a single point on this stroke

		:param p: (x, y)
		:return: parameter t at which p is closer to stroke
		"""
		o = self.linestring.project(Point(p), normalized=True)
		if isinstance(o, float):
			return o
		else:
			return o[0]

	def project_point(self, p):
		"""
		Project a single point on this stroke

		:param p: (x, y)
		:return: (x, y), point on the stroke where p projects
		"""
		t = self.project(p)
		return self.eval(t)

	def intersects(self, h_stroke):
		"""
		Check if this stroke intersects another one

		:param h_stroke: ShapelyStroke
		:return: bool
		"""
		return self.linestring.intersects(h_stroke.linestring)

	def get_intersection(self, h_stroke):
		"""
		Get the list of intersection points with another stroke

		:param h_stroke: ShapelyStroke
		:return: list of (x, y)
		"""
		try:
			i = self.linestring.intersection(h_stroke.linestring)
		except TopologicalError:
			print("Warning: trying to get intersection with a non-valid or complex geometry.")
			pass
			return []

		if isinstance(i, Point):
			return [(i.x, i.y)]
		elif isinstance(i, MultiPoint):
			return [(p.x, p.y) for p in i]
		else:
			return []

	def get_intersection_parameter(self, h_stroke):
		"""
		Compute parameters on this stroke where another stroke intersects.
		Be careful, it is not commutative.

		:param h_stroke: ShapelyStroke
		:return: list of t in [0->1]
		"""
		return [self.project(p) for p in self.get_intersection(h_stroke)]

	def intersects_buffer(self, h_stroke, buffer):
		"""
		Check if this stroke intersects another stroke within a given radius

		:param h_stroke: ShapelyStroke
		:param buffer: radius
		:return: bool
		"""
		return self.linestring.intersects(h_stroke.linestring.buffer(buffer))

	def get_intersection_buffer(self, h_stroke, buffer):
		"""
		Get the list of intersection points with another stroke, given an accuracy radius.
		All points returned belong to the calling stroke and are close to other stroke.
		Be careful, it is not commutative.

		:param h_stroke: ShapelyStroke
		:param buffer: radius
		:return: list of list of (x,y)
		"""
		#buffer_i = self.linestring.buffer(0.000001).intersection(h_stroke.linestring.buffer(buffer))
		i = self.linestring.intersection(h_stroke.linestring.buffer(buffer))
		if isinstance(i, LineString):
			return [[tuple(p) for p in i.coords]]
		elif isinstance(i, MultiLineString):
			return [[tuple(p) for p in line.coords] for line in i.geoms]
		else:
			return [[]]

	def get_intersection_parameter_buffer(self, h_stroke, buffer):
		"""
		Get the list of parameters of intersection points on this stroke
		with another stroke, given an accuracy radius.
		Be careful, it is not commutative.

		:param h_stroke:
		:param buffer:
		:return:
		"""
		out = []
		for line in self.get_intersection_buffer(h_stroke, buffer):
			params = [self.project(p) for p in line]
			if len(params) > 0:
				out.append([min(params), max(params)])
		return out

	def get_simplification(self, tolerance):
		"""
		Simply using RDP

		:param tolerance: RDP tolerance
		:return: the simplified line
		"""
		return self.linestring.simplify(tolerance)

	def plot(self, color):
		import matplotlib.pyplot as plt
		for i, p in enumerate(list(self.linestring.coords)):
			if i != 0:
				plt.plot([prev[0], p[0]], [prev[1], p[1]], color=color, linewidth=0.3)
			prev = p