from shapely.strtree import STRtree
from shapely.geometry import Point, LineString, MultiLineString
import numpy as np
import time
import networkx as nx
from matplotlib import pylab as pl
from matplotlib import pyplot as plt
from matplotlib.patches import Circle

def get_intersection_buffer(l1, l2, buffer):
	"""
    Get the list of intersection points with another stroke, given an accuracy radius.
    All points returned belong to the calling stroke and are close to other stroke.
    Be careful, it is not commutative.

    :param h_stroke: ShapelyStroke
    :param buffer: radius
    :return: list of list of (x,y)
    """
	i = l1.intersection(l2.buffer(buffer))
	if isinstance(i, LineString):
		return [[tuple(p) for p in i.coords]]
	elif isinstance(i, MultiLineString):
		return [[tuple(p) for p in line.coords] for line in i]
	else:
		return [[]]

def common_member(a, b):
	a_set = set(a)
	b_set = set(b)
	return a_set & b_set

# inter_params and mid_inter_param are 2x2 and 2x1 arrays respectively, as you
# have intersection parameters for both strokes
# the first row indices parameters on stroke stroke_ids[0]
# the snd row indices parameters on stroke stroke_ids[1]
class EdgeIntersection:
	def __init__(self, inter_id, inter_params, mid_inter_param, inter_coords,
				 stroke_ids, acc_radius, adjacent_inter_ids=None):
		self.inter_id = inter_id
		self.inter_params = inter_params
		self.mid_inter_param = mid_inter_param
		self.inter_coords = inter_coords
		self.stroke_ids = stroke_ids
		self.acc_radius = acc_radius
		self.adjacent_inter_ids = adjacent_inter_ids
		self.adjacency_cluster = -1

	def __str__(self):
		return "Inter_id: "+str(self.inter_id) +", strokes: "+str(self.stroke_ids)

class NodeStroke:
	def __init__(self, stroke_id, inter_ids):
		self.stroke_id = stroke_id
		self.inter_ids = inter_ids

class IntersectionGraph:

	def __init__(self):
		self.strokes = []
		self.adjacency_mat = np.array([])
		self.edge_intersections = dict()
		self.node_strokes = []
		self.stroke_inter_ids = []
		self.inter_ids_between_two_strokes = []
		self.intersection_by_stroke_ids_table = None
		self.adjacency_clusters = []

	def compute_stroke_adjacency_mat(self):
		linestrings = [s.linestring.linestring for s in self.strokes]
		extended_linestrings = [s for s in self.extended_strokes]
		tree = STRtree(linestrings)
		extended_tree = STRtree(extended_linestrings)
		self.adjacency_mat = np.zeros([len(linestrings), len(linestrings)], dtype=np.bool)
		index_by_id = dict((id(l), i) for i, l in enumerate(linestrings))
		extended_index_by_id = dict((id(l), i) for i, l in enumerate(extended_linestrings))

		for l_id, l in enumerate(linestrings):
			# query_geom can be replaced by l.buffer(epsilon)
			acc_radius = self.strokes[l_id].get_acc_radius()
			if acc_radius is None:
				acc_radius = 0.0
			query_geom = l.buffer(acc_radius)
			#l_neighbours = [index_by_id[id(inter_l)] for inter_l in tree.query(query_geom)
			#				if query_geom.intersects(inter_l)]
			l_neighbours = [inter_l for inter_l in tree.query(query_geom)
							if query_geom.intersects(linestrings[inter_l])]
			l_extended_neighbours = []
			if self.strokes[l_id].axis_label != 5:
				extended_query_geom = extended_linestrings[l_id].buffer(acc_radius)
				#l_extended_neighbours = [extended_index_by_id[id(inter_l)]
				#						 for inter_l in extended_tree.query(extended_query_geom)
				#						 if extended_query_geom.intersects(inter_l)]
				l_extended_neighbours = [inter_l
										 for inter_l in extended_tree.query(extended_query_geom)
										 if extended_query_geom.intersects(extended_linestrings[inter_l])]
			#l_neighbours = l_neighbours[l_neighbours < l_id].tolist()
			self.adjacency_mat[l_id, l_neighbours] = True
			self.adjacency_mat[l_neighbours, l_id] = True
			for l_ext_neigh in l_extended_neighbours:
				if self.strokes[l_ext_neigh].axis_label != 5:
					self.adjacency_mat[l_id, l_ext_neigh] = True
					self.adjacency_mat[l_ext_neigh, l_id] = True
			# remove intersection with yourself
			self.adjacency_mat[l_id, l_id] = False
		#for i in range(self.adjacency_mat.shape[0]):
		#	for j in range(i+1, self.adjacency_mat.shape[0]):


	def get_stroke_neighbours(self, s_id):
		return np.argwhere(self.adjacency_mat[s_id, :] == True).flatten()

	def gather_intersections(self):

		self.edge_intersections = dict()
		inter_id = 0
		for s_id, s in enumerate(self.strokes):
			neighbours = self.get_stroke_neighbours(s_id)
			for neighbour in neighbours:
				if neighbour >= s_id:
					continue
				max_acc_radius = max(s.get_acc_radius(), self.strokes[neighbour].get_acc_radius())
				inter_lines = []
				if s.axis_label != 5 and self.strokes[neighbour].axis_label != 5:
					inter_lines = get_intersection_buffer(
						self.extended_strokes[neighbour],
						self.extended_strokes[s_id], max_acc_radius)
				else:
					inter_lines = self.strokes[neighbour].get_intersection_fuzzy(s, max_acc_radius)
					#s, s.get_acc_radius())
				# get parameters, lying on both lines
				first_inter_params = []
				snd_inter_params = []
				ext_snd_inter_params = []
				for inter_line in inter_lines:
					first_params = [self.strokes[neighbour].linestring.project(Point(p)) for p in inter_line]
					snd_params = [s.linestring.project(Point(p)) for p in inter_line]
					ext_snd_params = [self.extended_strokes[s_id].project(Point(p), normalized=True) for p in inter_line]
					if len(first_params) > 0:
						first_inter_params.append([min(first_params), max(first_params)])
					if len(snd_params) > 0:
						snd_inter_params.append([min(snd_params), max(snd_params)])
						ext_snd_inter_params.append([min(ext_snd_params), max(ext_snd_params)])
				first_mid_inter_params = [(inter_param[0]+inter_param[-1])/2.0 for inter_param in first_inter_params]
				if self.strokes[neighbour].is_ellipse():
					first_mid_inter_params = [inter_param[0] for inter_param in first_inter_params]
				snd_mid_inter_params = [(inter_param[0]+inter_param[-1])/2.0 for inter_param in snd_inter_params]
				ext_snd_mid_inter_params = [(inter_param[0]+inter_param[-1])/2.0 for inter_param in ext_snd_inter_params]
				if s.is_ellipse():
					snd_mid_inter_params = [inter_param[0] for inter_param in snd_inter_params]
				#inter_coords = [s.eval_polyline(mid_inter_param) for mid_inter_param in snd_mid_inter_params]
				#if not self.strokes[neighbour].is_ellipse() and s.is_ellipse():
				#	inter_coords = np.array([self.strokes[neighbour].eval_polyline(mid_inter_param).coords[0] for mid_inter_param in first_mid_inter_params])
				#else:
				#	inter_coords = [np.array(self.extended_strokes[s_id].interpolate(mid_inter_param, normalized=True).coords[0])
				#					for mid_inter_param in ext_snd_mid_inter_params]
				if not self.strokes[neighbour].is_ellipse() and s.is_ellipse():
					inter_coords = np.array([self.strokes[neighbour].eval_polyline(mid_inter_param) for mid_inter_param in first_mid_inter_params])
				else:
					inter_coords = [np.array(self.extended_strokes[s_id].interpolate(mid_inter_param, normalized=True).coords[0])
									for mid_inter_param in ext_snd_mid_inter_params]
				for i in range(len(first_inter_params)):
					self.edge_intersections[inter_id] = EdgeIntersection(inter_id=inter_id,
																	inter_params=[first_inter_params[i],
																	 snd_inter_params[i]],
																	mid_inter_param=[first_mid_inter_params[i],
																	 snd_mid_inter_params[i]],
																	inter_coords=inter_coords[i],
																	stroke_ids=[neighbour, s_id],
																	acc_radius=max_acc_radius)
					inter_id += 1

	def init_intersection_graph(self, strokes):
		self.strokes = strokes
		self.extended_strokes = []
		for s in strokes:
			ext_s = LineString([p.coords for p in s.points_list])
			if s.axis_label != 5:
				vec = np.array(s.points_list[-1].coords) - np.array(s.points_list[0].coords)
				new_geom = [np.array(s.points_list[0].coords) - 0.25*vec,
							np.array(s.points_list[-1].coords) + 0.25*vec,]
				ext_s = LineString(new_geom)
			self.extended_strokes.append(ext_s)
		self.stroke_inter_ids = [[] for i in range(len(self.strokes))]
		self.compute_stroke_adjacency_mat()
		self.gather_intersections()
		# go trough all intersections and add the inter_id to stroke_inter_ids
		# of the participating strokes
		for inter in self.edge_intersections.values():
			self.stroke_inter_ids[inter.stroke_ids[0]].append(inter.inter_id)
			self.stroke_inter_ids[inter.stroke_ids[1]].append(inter.inter_id)

		self.compute_adjacent_intersections()
		self.translate_graph()

		# prepare table to efficiently retrieve intersections given two strokes
		self.intersection_by_stroke_ids_table = np.zeros([len(self.strokes), len(self.strokes)], dtype=np.int)
		self.intersection_by_stroke_ids_table[:, :] = -1
		self.inter_ids_between_two_strokes = []
		for i in range(len(self.strokes)):
			for j in range(i+1, len(self.strokes)):
				inter_ids = np.unique(self.stroke_inter_ids[i] + self.stroke_inter_ids[j])
				inter_ids = [k for k in inter_ids if np.sum(np.in1d(self.edge_intersections[k].stroke_ids, [i,j])) == 2]
				if len(inter_ids) == 0:
					continue
				self.intersection_by_stroke_ids_table[i][j] = len(self.inter_ids_between_two_strokes)
				self.intersection_by_stroke_ids_table[j][i] = len(self.inter_ids_between_two_strokes)
				self.inter_ids_between_two_strokes.append(inter_ids)
		self.compute_connected_clusters()

	def compute_adjacent_intersections(self):

		points = [Point(inter.inter_coords) for inter in self.edge_intersections.values()]
		points_inter_ids = [inter.inter_id for inter in self.edge_intersections.values()]
		index_by_id = dict((id(p), points_inter_ids[i]) for i, p in enumerate(points))
		tree = STRtree(points)

		for inter in self.edge_intersections.values():
			inter_id = inter.inter_id
			query_geom = points[inter_id].buffer(inter.acc_radius)
			#inter_neighbours = [index_by_id[id(inter_p)] for inter_p in
			#				tree.query(query_geom)
			#				if query_geom.intersects(inter_p) and \
			#					np.sum(np.in1d(self.edge_intersections[index_by_id[id(inter_p)]].stroke_ids, inter.stroke_ids)) > 0]
			inter_neighbours = [inter_p for inter_p in
							tree.query(query_geom)
							if query_geom.intersects(points[inter_p]) and \
								np.sum(np.in1d(self.edge_intersections[points_inter_ids[inter_p]].stroke_ids, inter.stroke_ids)) > 0]
			self.edge_intersections[inter_id].adjacent_inter_ids = inter_neighbours

	def translate_graph(self):
		# translate graph to class elements to be shared via the API
		self.node_strokes = [NodeStroke(stroke_id=s_id,
										inter_ids=self.stroke_inter_ids[s_id])
							 for s_id in range(len(self.strokes))]

	def get_intersections(self, inter_ids=[]):
		if len(inter_ids) > 0:
			return [e for e in self.edge_intersections.values() if e.inter_id in inter_ids]
		return list(self.edge_intersections.values())

	def get_adjacent_intersections(self, inter_id):
		return [self.edge_intersections[i] for i in self.edge_intersections[inter_id].adjacent_inter_ids]

	def get_strokes(self, stroke_ids=[]):
		if len(stroke_ids) > 0:
			return [self.node_strokes[i] for i in stroke_ids]
		return self.node_strokes

	# if you want to fetch all strokes belonging to a set of intersections
	def get_strokes_by_inter_ids(self, inter_ids=[]):
		return [n for n in self.node_strokes if common_member(n.inter_ids, inter_ids)]

	def strokes_intersect(self, stroke_id1, stroke_id2):
		return self.intersection_by_stroke_ids_table[stroke_id1][stroke_id2] != -1

	def get_intersections_by_stroke_ids(self, stroke_id1, stroke_id2):
		if self.intersection_by_stroke_ids_table[stroke_id1][stroke_id2] == -1:
			print("Warning: No intersection between strokes "+str(stroke_id1) +" and "+str(stroke_id2))
			return None
		return [self.edge_intersections[i] for i in
				self.inter_ids_between_two_strokes[self.intersection_by_stroke_ids_table[stroke_id1][stroke_id2]]]

	def get_intersections_by_stroke_id(self, stroke_id):
		return [self.edge_intersections[i] for i in self.stroke_inter_ids[stroke_id]]

	def display_intersections(self,
						fig=None, ax=None):
		if fig == None or ax == None:
			fig, ax = plt.subplots()
			ax.set_xlim(0, self.width)
			ax.set_ylim(self.height, 0)

		for inter in self.edge_intersections.values():
			ax.add_artist(Circle(xy=inter.inter_coords, radius=inter.acc_radius))

	def remove_intersection(self, del_inter_id):
		self.edge_intersections.pop(del_inter_id, None)
		for inter_id in self.edge_intersections.keys():
			if del_inter_id in self.edge_intersections[inter_id].adjacent_inter_ids:
				self.edge_intersections[inter_id].adjacent_inter_ids.remove(del_inter_id)
		for stroke_id, stroke in enumerate(self.node_strokes):
			if del_inter_id in stroke.inter_ids:
				self.node_strokes[stroke_id].inter_ids.remove(del_inter_id)
		for stroke_id, stroke_ids in enumerate(self.stroke_inter_ids):
			if del_inter_id in stroke_ids:
				self.stroke_inter_ids[stroke_id].remove(del_inter_id)
		for vec_id, inter_ids in enumerate(self.inter_ids_between_two_strokes):
			if del_inter_id in inter_ids:
				self.inter_ids_between_two_strokes[vec_id].remove(del_inter_id)

	def remove_intersections(self, del_inter_ids):
		for del_inter_id in del_inter_ids:
			self.remove_intersection(del_inter_id)

		# update global structures
		stroke_neighbors = np.argwhere(self.intersection_by_stroke_ids_table > -1)
		for [s_1, s_2] in stroke_neighbors:
			if len(self.inter_ids_between_two_strokes[self.intersection_by_stroke_ids_table[s_1][s_2]]) == 0 or \
					len(self.inter_ids_between_two_strokes[self.intersection_by_stroke_ids_table[s_2][s_1]]) == 0:
				self.adjacency_mat[s_1][s_2] = False
				self.adjacency_mat[s_2][s_1] = False
				self.intersection_by_stroke_ids_table[s_1][s_2] = -1
				self.intersection_by_stroke_ids_table[s_2][s_1] = -1

		self.compute_connected_clusters()

	def compute_connected_clusters(self):
		adj_graph = nx.Graph()

		for inter in self.edge_intersections.values():
			for adj_inter_id in inter.adjacent_inter_ids:
				adj_graph.add_edge(inter.inter_id, adj_inter_id)
		clusters = [list(comp) for comp in nx.connected_components(adj_graph)]
		self.adjacency_clusters = clusters
		for inter in self.edge_intersections.values():
			for c_id, c in enumerate(clusters):
				if inter.inter_id in c:
					self.edge_intersections[inter.inter_id].adjacency_cluster = c_id

	def are_adjacent_inter_ids(self, inter_id_1, inter_id_2):
		if self.edge_intersections[inter_id_1].adjacency_cluster == -1 or \
				self.edge_intersections[inter_id_2].adjacency_cluster == -1:
			return False
		return self.edge_intersections[inter_id_1].adjacency_cluster == self.edge_intersections[inter_id_2].adjacency_cluster

	#def are_adjacent_inter_ids(self, inter_id_1, inter_id_2):
	#	return inter_id_1 in self.edge_intersections[inter_id_2].adjacent_inter_ids or \
	#		   inter_id_2 in self.edge_intersections[inter_id_1].adjacent_inter_ids
