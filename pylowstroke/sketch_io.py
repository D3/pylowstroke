import json
import numpy as np
from xml.dom import minidom
from cairosvg import svg2png

from pylowstroke.sketch_tools import *
from pylowstroke.sketch_core import *
from svgpathtools import parse_path


# create list of strokes from a txt used by Pascal Barla
def read_barla_txt(file):
	f = open(file)
	polylines = []
	poly = []
	for line in f:
		if is_number(line):
			next = f.readline()
			if not is_number(next): raise ValueError("Not a number")
			pt = (float(line), float(next))
			poly.append( pt )
		else:
			if poly != []:
				polylines.append(list(poly))
				poly = []

	sk = Sketch()
	sk.strokes = [Stroke([StrokePoint(pt[0],pt[1]) for pt in poly]) for poly in polylines]
	return sk




class SketchSerializer:
	"""
	Load or save a skatehc from disk. Formats supported are : json, svg, png (save only)
	"""
	@staticmethod
	def load(filename, keep_duplicate_points=False):
		"""
		Load a sketch from file system. Supported formats : json, svg

		:type filename: string
		:return: Sketch
		"""
		if check_file_ext(filename, '.svg'):
			svg = minidom.parse(filename)
			return SketchSerializer._from_svg(svg)
		elif check_file_ext(filename, '.json'):
			jsondata = json.load(open(filename))
			return SketchSerializer._from_json(jsondata, keep_duplicate_points)
		else:
			raise ValueError("Unknown extension : " + filename)
	@staticmethod
	def save(sketch, filename, background=False, opacity=True, line_width=True, title=""):
		"""
		Save a sketch to file system. Supported formats : json, svg, png

		:type sketch: Sketch
		:type filename: string
		:param background: set to true to add a white background (svg or png only)
		:param opacity: set to true to command opacity with pressure data (svg only)
		:param line_width: set to true to command line width with pressure data (svg only)
		"""
		if check_file_ext(filename, '.svg'):
			svg = SketchSerializer._to_svg(sketch, background, opacity, line_width, title)
			with open(filename, 'w') as f:
				xml = svg.toprettyxml()
				f.write(xml)
				f.close()  
		elif check_file_ext(filename, '.json'):
			jsondata = SketchSerializer._to_json(sketch)
			with open(filename, 'w') as f:
				json.dump(jsondata, f, indent=4)
				f.close()
		elif check_file_ext(filename, '.png'):
			svg = SketchSerializer._to_svg(sketch, background, opacity, line_width)
			svg = svg.toprettyxml()
			svg2png(bytestring=svg, write_to=filename, output_width=2000, output_height=2000)
		else:
			raise ValueError("Unknown extension : " + filename)

	@staticmethod
	def _from_json(jsondata, keep_duplicate_points=False):
		s = Sketch()
		s.width = int(jsondata['canvas']['width'])
		s.height = int(jsondata['canvas']['height'])
		s.pen_width = jsondata['canvas']['pen_width']

		s.strokes = []
		for strokedata in jsondata['strokes']:
			if strokedata['is_removed'] : continue
			if len(strokedata['points']) < 2: continue
			stroke = SketchSerializer.StrokeSerializer.load(strokedata, 'JSON', keep_duplicate_points)
			if len(stroke.points_list) == 0:
				continue
			if stroke != None:
				stroke.set_width(s.pen_width)
				s.strokes.append(stroke)
		s.update_stroke_indices()

		return s

	@staticmethod
	def _to_json(sketch):
		jsondata = {}
		jsondata['canvas'] = {}
		jsondata['canvas']['height'] = sketch.height
		jsondata['canvas']['width'] = sketch.width
		jsondata['canvas']['pen_width'] = sketch.pen_width

		jsondata['strokes'] = []
		for stroke in sketch.strokes:
			s = SketchSerializer.StrokeSerializer.to(stroke, 'JSON')
			jsondata['strokes'].append(s)

		return jsondata

	@staticmethod
	def _svg_parser(node):
		# if node.nodeType != 1: return

		sketch_data = {}
		sketch_data['canvas'] = {}
		sketch_data['canvas']['updated'] = False
		sketch_data['strokes'] = []

		# parsing this node
		if node.nodeName == 'svg':
			if node.hasAttribute("height") and node.hasAttribute("width"):
				sketch_data['canvas']['height'] = float(node.getAttribute("height"))
				sketch_data['canvas']['width'] = float(node.getAttribute("width"))
			elif node.hasAttribute("viewBox"):
				viewbox = node.getAttribute("viewBox").split()
				sketch_data['canvas']['width'] = float(viewbox[2]) - float(viewbox[0])
				sketch_data['canvas']['height'] = float(viewbox[3]) - float(viewbox[1])
			if node.hasAttribute("pen_width"):
				sketch_data['canvas']['pen_width'] = float(node.getAttribute("pen_width"))
			else:
				print("Warning: parsing SVG with no pen_width info")
				sketch_data['canvas']['pen_width'] = 1.5
			sketch_data['canvas']['updated'] = True
		elif node.nodeName == 'polyline':
			stroke = SketchSerializer.StrokeSerializer.load(node, 'POLY')
			if stroke != None:
				sketch_data['strokes'].append(stroke)
		elif node.nodeName == 'path':
			stroke = SketchSerializer.StrokeSerializer.load(node, 'PATH')
			if stroke != None:
				sketch_data['strokes'].append(stroke)
		# else:
		# 	print("Ignoring "+node.nodeName+" element")

		# parsing children nodes
		for child in node.childNodes:
			child_data = SketchSerializer._svg_parser(child)
		
			# concatenate info
			sketch_data['strokes'] += child_data['strokes']
			if child_data['canvas']['updated']:
				sketch_data['canvas'] = child_data['canvas']

		return sketch_data

	@staticmethod
	def _from_svg(svg):
		sketch_data = SketchSerializer._svg_parser(svg)

		sketch = Sketch()
		sketch.height = sketch_data['canvas']['height']
		sketch.width = sketch_data['canvas']['width']
		if 'pen_width' in sketch_data['canvas'].keys():
			sketch.pen_width = sketch_data['canvas']['pen_width']
		else:
			sketch.pen_width = 1.0
		sketch.strokes = sketch_data['strokes']
		sketch.update_stroke_indices()

		return sketch       

	@staticmethod
	def _to_svg(sketch, background=False, opacity=True, line_width=True, title=""):
		svg = minidom.Document()

		svg_node = svg.createElement('svg')
		svg_node.setAttribute('width', str(sketch.width))
		svg_node.setAttribute('height', str(sketch.height))
		svg_node.setAttribute('pen_width', str(sketch.pen_width))
		svg_node.setAttribute('xmlns','http://www.w3.org/2000/svg')
		svg.appendChild(svg_node)

		if background:
			rect = svg.createElement('rect')
			rect.setAttribute("width", str(sketch.width));
			rect.setAttribute("height", str(sketch.height));
			rect.setAttribute("style", "fill:rgb(255,255,255)");
			svg_node.appendChild(rect)

		if title != "":
			text = svg.createTextNode(title)
			text_node = svg.createElement('text')
			text_node.appendChild(text)
			text_node.setAttribute('text-anchor', "middle")
			text_node.setAttribute('font-size', "50")
			text_node.setAttribute('x', "500")
			text_node.setAttribute('y', "900")
			svg_node.appendChild(text_node)

		for stroke in sketch.strokes:
			poly_node = SketchSerializer.StrokeSerializer.to(stroke, 'POLY', opacity=opacity, line_width=line_width)
			svg_node.appendChild(poly_node)

		return svg

	class StrokeSerializer:
		@staticmethod
		def load(data, format, keep_duplicate_points=False):
			if format == 'PATH':
				return SketchSerializer.StrokeSerializer._load_path(data)
			if format == 'POLY':
				return SketchSerializer.StrokeSerializer._load_poly(data)
			elif format == 'JSON':
				return SketchSerializer.StrokeSerializer._load_json(data, keep_duplicate_points)

		@staticmethod
		def to(data, format, opacity=True, line_width=True):
			if format == 'POLY':
				return SketchSerializer.StrokeSerializer._to_poly(data, opacity, line_width)
			elif format == 'JSON':
				return SketchSerializer.StrokeSerializer._to_json(data)

		@staticmethod
		def _load_json(jsondata, keep_duplicate_points=False):
			points = []
			keys = set()
			prev = None

			# reject empty strokes
			if isinstance(jsondata['points'], dict):
				return Stroke([], avail_keys=keys, sort_key='time')
			for point in jsondata['points']:
				p = StrokePoint(point['x'], point['y'])
				if not keep_duplicate_points and prev != None and p.eq_coords(prev):
					continue
				for k, v in point.items():
					if k == 'p':
						p.add_data('pressure', v)
						keys.add('pressure')
					elif k == 't':
						p.add_data('time', v)
						keys.add('time')
				points.append(p)
				prev = p

			if len(points) >= 2:
				return Stroke(points, avail_keys=keys, sort_key='time')
			else:
				return None

		@staticmethod
		def _to_json(stroke):
			s = {}
			s['is_removed'] = False
			s['points'] = []
			for point in stroke.points_list:
				p = {}
				p['x'] = point.coords[0]
				p['y'] = point.coords[1]
				try:
					p['p'] = point.get_data('pressure')
				except KeyError:
					p['p'] = 1.0
				try:
					p['t'] = point.get_data('time')
				except KeyError:
					p['t'] = 0.0
				s['points'].append(p)

			return s

		@staticmethod
		def _load_path(pathdata):
			path = parse_path(pathdata.getAttribute("d"))
			coords = [path.point(t) for t in np.linspace(0, 1.0, 10)]
			points_list = [StrokePoint(c.real, c.imag) for c in coords if not c is None]
			for pt_id in range(len(points_list)):
				points_list[pt_id].add_data("pressure", 1.0)
			stroke = Stroke(points_list, 1.0)
			return stroke

		@staticmethod
		def _load_poly(polydata):
			points_list = []
			width = 1.0
			data = {}
			coords = []

			possible_attributes = ['pressure', 'time', 'curvature_circle', 'speed', 'fit_curvature',
								   'fit_curvature_derivative', 'fit_theta']

			for a in polydata.attributes.keys():
				if a == 'points':
					points = polydata.getAttribute('points')
					if ',' in points:
						points = points.split()
						points = map(lambda p: tuple(map(float, p.split(','))), points)
						coords = [[x[0], x[1]] for x in points]
					else:
						points = points.split()
						i = 0
						coords = []
						while i < len(points):
							coords.append([points[i], points[i + 1]])
							i += 2
				elif a == 'stroke-width':
					width = float(polydata.getAttribute('stroke-width'))
				if a == "style":
					atts = polydata.getAttribute(a)
					if "rgba" in atts:
						data["pressure"] = np.repeat(np.fromstring(atts.split("rgba(")[1].split(");")[0], sep=",")[-1], len(points))
					if "width" in atts:
						width_str = atts.split("width:")[1].split(";")[0]
						if "px" in width_str:
							width = float(width_str.split("px")[0])
						else:
							width = float(width_str)
				elif a in possible_attributes:
					data[a] = polydata.getAttribute(a).split()
			# elif a == 'display':
			# 	if polydata.getAttribute("display") == "none":
			# 		is_removed = True

			# remove adjacent duplicate points
			indices = list(delete_consecutive_duplicates(coords))

			if len(indices) < 2:
				return None

			# stroke.removed = is_removed
			for i in indices:
				c = coords[i]
				pt = StrokePoint(c[0], c[1])
				for k, v in data.items():
					pt.add_data(k, float(v[i]))
				points_list.append(pt)

			if 'time' in data:
				stroke = Stroke(points_list, width, sort_key='time', avail_keys=set(data.keys()))
			else:
				stroke = Stroke(points_list, width, avail_keys=set(data.keys()))

			return stroke

		@staticmethod
		def _to_poly(stroke, pressure_opacity, pressure_line_width):
			svg = minidom.Document()

			pts_str = ""

			data_strings = {}
			for k in stroke.avail_keys:
				data_strings[k] = ''

			for p in stroke.points_list:
				pts_str += str(p.coords[0])
				pts_str += ','
				pts_str += str(p.coords[1])
				pts_str += ' '

				for k, v in p.data.items():
					data_strings[k] += str(v) + ' '

			poly_node = svg.createElement('polyline')
			poly_node.setAttribute('points', pts_str)

			if hasattr(stroke, "svg_color"): 
				style_string = 'fill:none; stroke:'+str(stroke.svg_color)+';'
			else : 
				style_string = 'fill:none; stroke:black;'
			#style_string = 'fill:none; stroke:black; '

			for k, v in data_strings.items():
				poly_node.setAttribute(k, v)

			if stroke.has_data('pressure'):
				mean_pressure = stroke.get_mean_data('pressure')
				if pressure_opacity:
					style_string += 'stroke-opacity:%s; ' % str(mean_pressure)
				if pressure_line_width:
					style_string += 'stroke-width:%s; ' % str(stroke.width * mean_pressure)
				else:
					style_string += 'stroke-width:%s; ' % str(stroke.width)
			else:
				style_string += 'stroke-width:%s; ' % str(stroke.width)

			poly_node.setAttribute('style', style_string)

			return poly_node


if __name__ == "__main__":
	s = SketchSerializer.load("v1_01.json")
	SketchSerializer.save(s, "test.svg")

	s = SketchSerializer.load("test.svg")
	s = SketchSerializer.load("test.svg")
	SketchSerializer.save(s, "test.json")

	s = SketchSerializer.load("test.json")
	SketchSerializer.save(s, "test.png", background=True, line_width=False, opacity=False)

	#s = SketchSerializer.load("cube_polyline.svg")
	#SketchSerializer.save(s, "cube.json")
	#SketchSerializer.save(s, "cube_test_output.svg")
	#SketchSerializer.save(s, "cube_test_output.png", background=True, line_width=False, opacity=False)
