from setuptools import setup

setup(
   name='pylowstroke',
   version='1.0',
   description='Low-level stroke processing library',
   author='Felix Hahnlein, Bastien Wailly',
   author_email='felix.hahnlein@inria.fr',
   packages=['pylowstroke'],
   include_package_data=False,
   #package_data={'': [""]},
)
